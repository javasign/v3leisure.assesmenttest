﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using V3Leisure.TestWS.DataStructures;
using V3Leisure.TestWS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace TestWS.Tests {
    [TestClass()]
    public class ServiceDistributorTests {
        [TestMethod()]
        //
        // Add item with valid value
        // Expected: ok
        //
        public void AddTest() {
            ServiceDistributor target = new ServiceDistributor();
            AddRequestResult result = target.Add("HelloWorld", 1);

            Assert.IsTrue(result.result, result.message);
            Assert.AreNotEqual<Guid>(Guid.Empty, result.id);
            Assert.IsTrue(TestDataSource._dataTable.Select("BODS_Id = '" + result.id + "'").Length > 0);
        }

        [TestMethod()]
        //
        // Add item with empty fullname
        // Expected: rejected
        //
        public void AddEmptyTest() {
            ServiceDistributor target = new ServiceDistributor();
            AddRequestResult result = target.Add("", 1);

            Assert.IsFalse(result.result, result.message);
        }

        [TestMethod()]
        //
        // Add item with not valid status
        // Expected: rejected
        //
        public void AddInvalidStatusTest() {

            ServiceDistributor target = new ServiceDistributor();
            AddRequestResult result = target.Add("HelloWorld", 5);

            Assert.IsFalse(result.result, result.message);
        }

        [TestMethod()]
        //
        // Update with exists id and valid value
        // Expected: ok
        //
        public void UpdateTest() {
            ServiceDistributor target = new ServiceDistributor();
            RequestResult result = target.Update(new Guid("617dba9d-391b-4ca7-aeeb-0703ca526709"), "HelloWorld", 3);

            Assert.IsTrue(result.result, result.message);
        }

        [TestMethod()]
        //
        // Update with not existed id
        // Expected: rejected
        //
        public void UpdateNotExistsTest() {
           
            ServiceDistributor target = new ServiceDistributor();
            RequestResult result = target.Update(new Guid("617dba9d-391b-4ca7-0000-0703ca526709"), "HelloWorld", 2);

            Assert.IsFalse(result.result, result.message);
        }

        [TestMethod()]
        //
        // Update with existed id and empty value
        // Expected: ok, but nothing changed
        //
        public void UpdateNullFullNameAndStatusTest() {
            ServiceDistributor target = new ServiceDistributor();

            DataRow origin = TestDataSource._dataTable.Select("BODS_Id = '617dba9d-391b-4ca7-aeeb-0703ca526709'")[0];

            RequestResult result1 = target.Update(new Guid("617dba9d-391b-4ca7-aeeb-0703ca526709"), null, -1);
            DataRow row1 = TestDataSource._dataTable.Select("BODS_Id = '617dba9d-391b-4ca7-aeeb-0703ca526709'")[0];

            RequestResult result2 = target.Update(new Guid("617dba9d-391b-4ca7-aeeb-0703ca526709"));
            DataRow row2 = TestDataSource._dataTable.Select("BODS_Id = '617dba9d-391b-4ca7-aeeb-0703ca526709'")[0];

            if (result1.result && result2.result) {
                Assert.AreSame(origin, row1);
                Assert.AreSame(origin, row2);
            } else Assert.Fail();
        }

        [TestMethod()]
        //
        // Update with wrong id format
        // Expected: rejected
        //
        public void UpdateInvalidIDTest() {
            ServiceDistributor target = new ServiceDistributor();
            RequestResult result = target.Update("invalid_guid", "NeverUpdate");

            Assert.IsFalse(result.result, result.message);
        }

        [TestMethod()]
        //
        // Get all records
        // Expected: ok, with all records
        //
        public void GetAllTest() {
            ServiceDistributor target = new ServiceDistributor();
            GetRequestResult result = target.Get("All");

            Assert.IsTrue(result.result, result.message);
            Assert.AreEqual<int>(TestDataSource._dataTable.Rows.Count, result.rows.Length);
        }

        [TestMethod()]
        //
        // Get record(s) with filter by ID
        // Expected: ok
        //
        public void GetByIDTest() {
            ServiceDistributor target = new ServiceDistributor();
            GetRequestResult result = target.Get("Id", "617dba9d-391b-4ca7-aeeb-0703ca526709");

            Assert.IsTrue(result.result);
            if (result.rows.Length > 0) {
                Assert.AreEqual<Guid>(new Guid("617dba9d-391b-4ca7-aeeb-0703ca526709"), result.rows[0].id);
            } else Assert.Fail();
        }

        [TestMethod()]
        //
        // Get record(s) with filter by FullName
        // Expected: ok, with records contain characters
        //
        public void GetByFullNameTest() {
            ServiceDistributor target = new ServiceDistributor();
            GetRequestResult result = target.Get("FullName", "e");

            Assert.IsTrue(result.result);
            if (result.rows.Length > 0) {
                foreach (Distributor i in result.rows) {
                    Assert.IsTrue(i.fullName.ToLower().IndexOf('e') >= 0);
                }
            } else Assert.Fail();
        }

        [TestMethod()]
        //
        // Get record(s) with filter by Status
        // Expected: ok
        //
        public void GetByStatusTest() {
            ServiceDistributor target = new ServiceDistributor();
            GetRequestResult result = target.Get("Status", "1");

            Assert.IsTrue(result.result);
            if (result.rows.Length > 0) {
                foreach (Distributor i in result.rows) {
                    Assert.IsTrue(i.status == 1);
                }
            } else Assert.Fail();
        }

        [TestMethod()]
        //
        // Get record(s) with unexisted id
        // Expected: ok, no records
        //
        public void GetNotExistsTest() {
            ServiceDistributor target = new ServiceDistributor();
            GetRequestResult result = target.Get("Id", "617dba9d-391b-4ca7-0000-0703ca526709");

            Assert.IsTrue(result.result);
            Assert.IsTrue(result.rows.Length <= 0);
        }
    }
}