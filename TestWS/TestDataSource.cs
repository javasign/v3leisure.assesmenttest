﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace V3Leisure.TestWS {
    public static class TestDataSource {
        public static DataTable _dataTable { get; private set; }

        static TestDataSource() {
            _dataTable = new DataTable();
            _dataTable.Columns.Add("BODS_Id", typeof(Guid)).AllowDBNull = false;
            _dataTable.Columns.Add("BODS_FullName", typeof(string)).AllowDBNull = false;
            _dataTable.Columns.Add("BODS_Status", typeof(byte)).AllowDBNull = false;
            _dataTable.PrimaryKey = new[] { _dataTable.Columns[0] };
            
            _dataTable.Rows.Add(new Guid("617dba9d-391b-4ca7-aeeb-0703ca526709"), "My Website", 1);
            _dataTable.Rows.Add(new Guid("bee3c0be-ccbf-4882-ad5a-d288f5677a51"), "Test V3 Offload", 2);
            _dataTable.Rows.Add(new Guid("5df458a8-a743-4e81-bf0d-bd874f6f0cd3"), "About Australia", 3);
            _dataTable.AcceptChanges();
        }
    }
}