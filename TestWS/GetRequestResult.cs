﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace V3Leisure.TestWS {
    public class GetRequestResult {
        public bool result                       = false;
        public string message                    = "";
        public DataStructures.Distributor[] rows = null;

        public GetRequestResult() {
            this.result = false;
            this.message = "Empty response";
            this.rows = new DataStructures.Distributor[] { };
        }

        public GetRequestResult(bool result, string message) {
            this.result = result;
            this.message = message;
            this.rows = new DataStructures.Distributor[] { };
        }

        public GetRequestResult(DataRow[] rows) {
            this.result = true;
            this.message = "";

            List<DataStructures.Distributor> tempRows = new List<DataStructures.Distributor>();
            if (rows != null) {
                for (int i = 0; i < rows.Length; i++) {
                    tempRows.Add(new DataStructures.Distributor() {
                        id = (Guid)rows[i]["BODS_Id"],
                        fullName = (string)rows[i]["BODS_FullName"],
                        status = (byte)rows[i]["BODS_Status"]
                    });
                }
            }
            this.rows = tempRows.ToArray();
        }
    }
}