﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace V3Leisure.TestWS {
    public class RequestResult {
        public bool result    = false;
        public string message = "";

        public RequestResult() {
            this.result = false;
            this.message = "";
        }

        public RequestResult(bool result, string message) {
            this.result = result;
            this.message = message;
        }
    }
}