﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace V3Leisure.TestWS {
    public class AddRequestResult {
        public Guid id        = Guid.Empty;
        public bool result    = false;
        public string message = "";

        public AddRequestResult() {
            this.result = false;
            this.message = "Empty response";
            this.id = Guid.Empty;
        }

        public AddRequestResult(bool result, string message) {
            this.result = result;
            this.message = message;
            this.id = Guid.Empty;
        }

        public AddRequestResult(Guid id) {
            this.result = true;
            this.message = "";
            this.id = id;
        }
    }
}